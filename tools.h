/*#################################################################################*\

    Developed by:			Jerry van Heerikhuize

    -----------------------------------------------------------------------------
    
    Version:                1.0.1
    Creation Date:          23/09/18
    Modification Date:      05/02/20
    Email:                  hello@halfbros.nl
    Description:            global tools file
    File:                   tools.h
    Licence:                GNU GPLv3

\*#################################################################################*/

void serialLog(String str, boolean newline = true, boolean debug = true){
    if (debug){
        Serial.print(millis());
        Serial.print(":");
        Serial.print(str);
        if (newline){
            Serial.println("");
        }
    }
}

String getSubString(String cmd, char seperator, int index){

    int chunkI = 0;
    int startI = 0;
    int endI = 0;
    int cmdLength = cmd.length();

    cmd += seperator;

    for (int i = 0; i < cmdLength; i++) {
        char character = cmd.charAt(i);
        if (character == ':'){
            chunkI++;
            endI = i;
            String chunk = cmd.substring(startI, endI);
            startI = endI + 1;
            if (chunkI == index){
                return chunk;
            }
        }
    }
    return "";
}
